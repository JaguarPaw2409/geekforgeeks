# INTERVIEW PREPARATION
## A) Data Structures and algorithms:
### Arrays (177)
    1. Basic (51) https://www.geeksforgeeks.org/basic/c-arrays/
    2. Easy (51) https://www.geeksforgeeks.org/easy/c-arrays/
    3. Medium (36) https://www.geeksforgeeks.org/medium/c-arrays/
    4. Hard (24) https://www.geeksforgeeks.org/hard/c-arrays/
    5. Extreme (15) https://www.geeksforgeeks.org/expert/c-arrays/
### Strings (165)
    1. Basic (51) https://www.geeksforgeeks.org/basic/strings/
    2. Easy (51) https://www.geeksforgeeks.org/easy/strings/
    3. Medium (36) https://www.geeksforgeeks.org/medium/strings/
    4. Hard (24) https://www.geeksforgeeks.org/hard/strings/
    5. Extreme (3) https://www.geeksforgeeks.org/expert/strings/
### Linked Lists (108)
    1. Basic (36) https://www.geeksforgeeks.org/basic/linked-list/
    2. Easy (36) https://www.geeksforgeeks.org/easy/linked-list/
    3. Medium (24) https://www.geeksforgeeks.org/medium/linked-list/
    4. Hard (10) https://www.geeksforgeeks.org/hard/linked-list/
    5. Extreme (2) https://www.geeksforgeeks.org/expert/linked-list/

### Binary Tree
### Binary Search Tree
### Heap
### Hashing
### Queue
### Stacks
### Matrix
### Graphs


## B) System Design (educative.io)
    1. https://www.educative.io/collection/5668639101419520/5649050225344512

## C) Python (GFG and Python 201)
    1. https://www.geeksforgeeks.org/python-programming-language/
    2. Python 201 Michael Driscoll

## D) Data Science
    1. Basics: Loonycorn
    2. Probability and Statistics basics
    3. Interview: Thedatamonk - 2 books
