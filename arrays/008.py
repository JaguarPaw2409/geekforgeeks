"""Search an element in a sorted and rotated array."""

"""An element in a sorted array can be found in O(log n) time via binary
search. But suppose we rotate an ascending order sorted array at some pivot
unknown to you beforehand. So for instance, 1 2 3 4 5 might become 3 4 5 1 2.
Devise a way to find an element in the rotated array in O(log n) time.

One approach is linear search with O(n^2) time complexity.

Best approach: O(logn)

1) Find Pivot element (Divide array into two sorted arrays)
   #) If array[start] < array[end] return 0
   #) mid = start + end / 2
      if (mid + 1) is pivot return (mid + 1)
      array(mid + 1) < array(mid)
   #) If array(start) < array(mid) First subarray is sorted, look for pivot in
      other part
      start = mid + 1
   #) Elif array(mid) < array(end) Second subarray is sorted, look for pivot in
      other part
      end = mid - 1
2) If number lies between start and pivot - 1, apply BS on that subarray
3) If number lies between pivot and end, apply BS on that subarray


Solution: https://www.youtube.com/watch?v=6pSzsJH56BA
"""

# Hard


def binary_search_num(arr, n):
    """BS algorithm to find the occurence of number."""
    start = 0
    end = len(arr)
    while start < end:
        mid = int((start + end) / 2)
        if arr[mid] == n:
            return 1
        elif arr[mid] < n:
            start = mid + 1
        else:
            end = mid - 1
    return -1


def find_pivot_index(arr):
    """Find pivot index."""
    start = 0
    end = len(arr) - 1
    if arr[start] < arr[end]:
        return 0
    while start < end:
        pivot = int((start + end) / 2)
        if arr[pivot + 1] < arr[pivot]:
            return pivot + 1
        elif arr[start] < arr[pivot]:  # First subarray is sorted
            start = pivot + 1
        else:
            end = pivot - 1
    return -1


def main(arr, n):
    """Find element in sorted and rotated array."""
    # Find pivot index
    pivot = find_pivot_index(arr)
    if arr[0] < arr[pivot - 1]:
        res = binary_search_num(arr[:pivot], n)
    else:
        res = binary_search_num(arr[pivot: len(arr)], n)
    print(res)


if __name__ == "__main__":
    A = [5, 6, 7, 8, 9, 10, 1, 2, 3]
    main(A, 3)
