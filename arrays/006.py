"""Ugly Numbers
Ugly numbers are numbers whose only prime factors are 2, 3 or 5.
The sequence 1, 2, 3, 4, 5, 6, 8, 9, 10, 12, 15, … shows the first 11
ugly numbers. By convention, 1 is included.

Given a number n, the task is to find n’th Ugly number.

Input  : n = 7
Output : 8

Input  : n = 10
Output : 12

Input  : n = 15
Output : 24

Input  : n = 150
Output : 5832."""

# Medium


def maximum_factor(num, val):
    """Divide the number as more as you can"""
    while num % val == 0:
        num /= val
    return num


def main(n):
    """Largest find next ugly number."""
    curr = 2  # 1 already included
    iter = 1
    while True:
        nu = maximum_factor(curr, 2)
        nu = maximum_factor(nu, 3)
        nu = maximum_factor(nu, 5)
        if nu == 1:
            iter += 1
        if iter == n:
            break
        curr += 1
    print(curr)
    return curr


if __name__ == "__main__":
    main(150)
