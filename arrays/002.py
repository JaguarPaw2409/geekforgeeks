"""Majority Element."""

"""Write a function which takes an array and prints the majority element
(if it exists), otherwise prints “No Majority Element”. A majority element in
an array A[] of size n is an element that appears more than n/2 times
(and hence there is at most one such element)."""

# Easy


def main(arr):
    """Get majority element."""
    temp = {}
    l_arr = len(arr)
    for a in arr:
        if temp.get(a):
            temp[a] += 1
        else:
            temp[a] = 1
        if temp[a] > l_arr / 2:
            print(a)
            return a
    print("No majority element.")
    """
    This will leads to increase in time complexity. Just check whether the
    count is greater than n/2

    srt = sorted(temp.items(), key=lambda x: x[1], reverse=True)
    print(srt[0][0])
    return srt[0][0]
    """


if __name__ == "__main__":
    A = [3, 3, 4, 2, 4, 4, 2, 4, 4]
    main(A)
