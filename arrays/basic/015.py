"""Program to find largest element in an array
Given an array, find the largest element in it.

Input : arr[] = {10, 20, 4}
Output : 20

Input : arr[] = {20, 10, 20, 4, 100}
Output : 100


Self explanatory.

"""

# Easy
