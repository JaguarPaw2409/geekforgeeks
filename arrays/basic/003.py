"""Find the smallest and second smallest elements in an array."""

"""
Solution: https://www.youtube.com/watch?v=M257z4V_ayM

"""

# Easy- Medium


def main(arr):
    """Smallest and second smallest number."""
    largest = arr[0]
    s_largest = arr[0]
    for i in range(1, len(arr)):
        if arr[i] > largest:
            s_largest = largest
            largest = arr[i]
    print(largest, s_largest)


if __name__ == "__main__":
    A = [12, 13, 1, 10, 34, 1]
    main(A)
