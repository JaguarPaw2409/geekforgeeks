"""Union and Intersection of two sorted arrays."""

"""Given two sorted arrays, find their union and intersection.

1) Use two index variables i and j, initial values i = 0, j = 0
2) If arr1[i] is smaller than arr2[j] then print arr1[i] and increment i.
3) If arr1[i] is greater than arr2[j] then print arr2[j] and increment j.
4) If both are same then print any of them and increment both i and j.
5) Print remaining elements of the larger array.

1) Use two index variables i and j, initial values i = 0, j = 0
2) If arr1[i] is smaller than arr2[j] then increment i.
3) If arr1[i] is greater than arr2[j] then increment j.
4) If both are same then print any of them and increment both i and j.


"""

# Medium


def union(arr1, arr2):
    """Union of two sorted arrays."""
    start1 = 0
    start2 = 0
    while True:
        if arr1[start1] < arr2[start2]:
            print(arr1[start1])
            start1 += 1
        elif arr2[start2] < arr1[start1]:
            print(arr2[start2])
            start2 += 1
        else:
            print(arr1[start1])
            start1 += 1
            start2 += 1
        if start1 > len(arr1) - 1:
            while start2 < len(arr2):
                print(arr2[start2])
                break
            break
        if start2 > len(arr2) - 1:
            while start1 < len(arr1):
                print(arr1[start1])
                break
            break


def intersection(arr1, arr2):
    """Intersection of two sorted arrays."""
    start1 = 0
    start2 = 0
    while True:
        if arr1[start1] < arr2[start2]:
            start1 += 1
        elif arr2[start2] < arr1[start1]:
            start2 += 1
        else:
            print(arr1[start1])
            start1 += 1
            start2 += 1
        if start1 > len(arr1) - 1 or start2 > len(arr2) - 1:
            break


if __name__ == "__main__":
    A = [1, 3, 4, 5, 7]
    B = [2, 3, 5, 6]
    union(A, B)
    intersection(A, B)
