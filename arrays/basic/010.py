"""Find a Fixed Point (Value equal to index) in a given array."""
"""Given an array of n distinct integers sorted in ascending order, write a
function that returns a Fixed Point in the array, if there is any Fixed Point
present in array, else returns -1. Fixed Point in an array is an index i such
that arr[i] is equal to i. Note that integers in array can be negative.

Examples:

  Input: arr[] = {-10, -5, 0, 3, 7}
  Output: 3  // arr[3] == 3

  Input: arr[] = {0, 2, 5, 8, 17}
  Output: 0  // arr[0] == 0


  Input: arr[] = {-10, -5, 3, 4, 7, 9}
  Output: -1  // No Fixed Point

  As elements are sorted use binary search for finding element at index n

"""

# Easy


def main(arr, n):
    """Binary search for searching element."""
    start = 0
    end = len(arr) - 1
    while start < end:
        mid = int((start + end) / 2)
        if mid == n:
            print(arr[n])
            return
        elif mid > n:
            end = mid - 1
        else:
            start = mid + 1
    print("Not found")


if __name__ == "__main__":
    A = [1, 3, 4, 5, 7]
    main(A, 2)
