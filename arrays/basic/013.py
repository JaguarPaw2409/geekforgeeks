""".Average of a stream of numbers."""

"""Given a stream of numbers, print average (or mean) of the stream at every
point. For example, let us consider the stream as 10, 20, 30, 40, 50, 60"""


# Easy


def main(n):
    """Average of stream of numbers."""
    sum = 0.0
    count = 0
    for i in range(1, n + 1):
        sum += (10 * i)
        count += 1
    print(sum/count)


if __name__ == "__main__":
    main(4)
