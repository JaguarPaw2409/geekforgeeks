"""Find the Number Occurring Odd Number of Times."""
"""
Given an array of positive integers. All numbers occur even number of times
except one number which occurs odd number of times."""

# Notes: Trivial approach O(n^2)
"""
The Best Solution is to do a bitwise XOR of all the elements. XOR of all
elements gives us odd occurring element. Please note that XOR of two elements
is 0 if both elements are same and XOR of a number x with 0 is x.

XOR (A, B) = A.B(bar) + A(bar).B
INPUT	OUTPUT
A	B	A XOR B
0	0	   0
0	1	   1
1	0	   1
1	1	   0


Your test array has these numbers:

2, 3, 5, 4, 5, 2, 4, 3, 5, 2, 4, 4, 2
Their counts are as follows:

2 - 4 times
3 - 2 times
4 - 4 times
5 - 3 times
Only 5 is listed an odd number of times.

XOR has these two properties:

Y ^ 0 = Y

X ^ X ^ Y = Y

For any value of X and Y. In other words, XOR-ing any number Y with zero leaves
the value unchanged, and XOR-ing a number X twice with any value Y leaves that
original value unchanged. The order of operations does not matter.
Since res starts at zero, XOR-ing together all numbers from your array
produce 5 - the only value that is not XOR-ed an even number of times.

"""

# Easy


def main(arr):
    """Get element with odd repetition."""
    temp = 0
    for a in arr:
        temp = a ^ temp
    print(temp)


if __name__ == "__main__":
    A = [1, 2, 3, 2, 3, 1, 3]
    main(A)
