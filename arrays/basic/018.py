"""
--- (Theory)

A data structure for n elements and O(1) operations
Propose a data structure for the following:
The data structure would hold elements from 0 to n-1.
There is no order on the elements (no ascending/descending order requirement)

The complexity of the operations should be as follows:
* Insertion of an element – O(1)
* Deletion of an element – O(1)
* Finding an element – O(1)



A boolean array works here. Array will have value ‘true’ at ith index if i is
present, and ‘false’ if absent.

Initialization:
We create an array of size n and initialize all elements as absent.
def initialize(n):
  bool A
  for i in range(0 , n)
    A[i] = 0


Insertion of an element:
def insert(i):
   A[i] = 1


Deletion of an element:
def delete(i)
  A[i] = 0

Finding an element:

def find(i)
{
    return A[i]
}

"""

# Medium
