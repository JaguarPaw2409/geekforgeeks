"""Segregate Even and Odd numbers."""
"""Given an array A[], write a function that segregates even and odd numbers.

Example

Input  = {12, 34, 45, 9, 8, 90, 3}
Output = {12, 34, 8, 90, 45, 9, 3}

"""

# Easy


def main(arr):
    """Segregate even and odd numbers."""
    start = 0
    end = len(arr) - 1
    while start < end:
        while arr[start] % 2 == 0 and start < end:
            start += 1
        while arr[end] % 2 != 0 and start < end:
            end -= 1
        if arr[start] % 2 != 0 and arr[end] % 2 == 0:
            arr[start], arr[end] = arr[end], arr[start]
            start += 1
            end -= 1
    print(arr)


if __name__ == "__main__":
    A = [12, 34, 45, 9, 8, 90, 3]
    main(A)
