"""Replace every element with the greatest element on right side."""

"""Given an array of integers, replace every element with the next greatest
element (greatest element on the right side) in the array. Since there is no
element next to the last element, replace it with -1. For example,
if the array is {16, 17, 4, 3, 5, 2}, then it should be modified to
{17, 5, 5, 5, 2, -1}.

This one is similar to leaders problem

"""

# Medium


def main(arr):
    """Replace element with greatest right element."""
    end = len(arr) - 1
    max = arr[end]
    print(-1)
    print(max)
    while end > 1:
        end -= 1
        if arr[end] > max:
            max = arr[end]
        print(max)


if __name__ == "__main__":
    A = [16, 17, 4, 3, 5, 2]
    main(A)
