"""Find the number of zeroes."""
"""
Given an array of 1s and 0s which has all 1s first followed by all 0s. Find
the number of 0s. Count the number of zeroes in the given array.

Examples :

Input: arr[] = {1, 1, 1, 1, 0, 0}
Output: 2

Input: arr[] = {1, 0, 0, 0, 0}
Output: 4

Input: arr[] = {0, 0, 0}
Output: 3

Input: arr[] = {1, 1, 1, 1}
Output: 0


1) Trivial approach: O(n)
2) As the array is sorted you can use Binary search for evaluating this count
O(logn)
"""

# Easy
