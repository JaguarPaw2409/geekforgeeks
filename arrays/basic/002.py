"""Find the Missing Number."""
"""
You are given a list of n-1 integers and these integers are in the range of 1
to n. There are no duplicates in list. One of the integers is missing in the
list. Write an efficient code to find the missing integer."""

# Medium


def main1(arr):
    """Finding missing number."""
    # Real sum of n+1 numbers - actual sum
    len_arr = len(arr)
    sum_n = ((len_arr + 1) * (len_arr + 2)) / 2  # n(n+1) / 2, One num is
    actual_sum = sum(arr)                        # missing, Hence (n+1)(n+2)/2
    print(sum_n - actual_sum)
    return (sum_n - actual_sum)


def main2(arr):
    """Finding missing number | XOR approach."""
    # (A1 ^ A2 ^ A4) ^ (A1 ^ A2 ^ A3 ^ A4)
    # (A1 ^ A1) ^ (A2 ^ A2) ^ (A3) ^ (A4 ^ A4)
    # 0 ^ 0 ^ A3 ^ 0
    x1 = arr[0]
    for i in range(1, len(arr)):
        x1 = arr[i] ^ x1

    # XOR of first n numbers
    x2 = 1
    for j in range(2, len(arr) + 2):
        x2 = j ^ x2
    print(x1 ^ x2)


if __name__ == "__main__":
    A = [1, 2, 4, 6, 3, 7, 8]
    main1(A)
    main2(A)
