"""Program to cyclically rotate an array by one
Given an array, cyclically rotate the array clockwise by one.

Examples:

Input:  arr[] = {1, 2, 3, 4, 5}
Output: arr[] = {5, 1, 2, 3, 4}
"""

# Easy


def main(arr):
    """Cyclic rotate array."""
    end = len(arr) - 1
    while end > 0:
        end -= 1
        arr[end], arr[end + 1] = arr[end + 1], arr[end]
    print(arr)


if __name__ == "__main__":
    A = [1, 2, 3, 4, 5]
    print(A)
    main(A)
