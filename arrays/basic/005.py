"""Leaders in an array."""
"""Write a program to print all the LEADERS in the array. An element is leader
if it is greater than all the elements to its right side. And the rightmost
element is always a leader. For example int the array {16, 17, 4, 3, 5, 2},
leaders are 17, 5 and 2.

Brute Force approach O(n^2)

Best approach O(n):
1) Traverse from end.
2) Print each max element when changes.
"""

# Medium


def main(arr):
    """Print leaders of array."""
    end = len(arr) - 1
    max = arr[end]
    print(max)
    while end >= 0:
        end -= 1
        if arr[end] > max:
            max = arr[end]
            print(max)


if __name__ == "__main__":
    A = [16, 17, 4, 3, 5, 2]
    main(A)
