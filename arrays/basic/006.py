"""Reverse array."""

# Easy

if __name__ == "__main__":
    A = [16, 17, 4, 3, 5, 2]
    start = 0
    end = len(A) - 1
    while start < end:
        A[start], A[end] = A[end], A[start]  # Swap
        start += 1
        end -= 1
    print(A)
