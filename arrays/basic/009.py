"""Linked list vs arrays. --- (Theory)"""


"""

--------------Arrays--------------- | --------------Linked Lists---------------
1) Fixed size                         1) Dynamic size
2) Insertion/Deletion diffcult        2) Easier Insertion and Deletion
3) Random access easier               3) Random access difficult.
4) No extras space for pointer.       4) Extras space to save pointer.
5) Better cache locality than LL.     5) Poor cache locality than arrays.

"""

# Easy
