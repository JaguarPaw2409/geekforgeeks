"""Segregate 0s and 1s in an array."""
"""
You are given an array of 0s and 1s in random order. Segregate 0s on left side
and 1s on right side of the array. Traverse array only once.

First approach:
1) Count number of zeros.
2) Fill with zero and len(arr) - count(0) with 1s.

This will traverse the array twice.

Best approach:
1) Traverse the array from both ends.
2) Keep incrementing left if value is 0, same with right for value 1.
3) If value at left pointer is greater than right pointer, then swap values.
"""

# Hard


def main(arr):
    """Segregate 0 and 1s."""
    start = 0
    end = len(arr) - 1
    while start < end:
        while arr[start] == 0 and start < end:
            start += 1
        while arr[end] == 1 and start < end:
            end -= 1
        if arr[start] > arr[end]:
            arr[start], arr[end] = arr[end], arr[start]
            start += 1
            end -= 1
    print(arr)
    return arr


if __name__ == "__main__":
    A = [0, 1, 0, 1, 0, 0, 1, 1, 1, 0]
    main(A)
