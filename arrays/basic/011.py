"""Program to find sum of elements in a given array."""

# Easy


if __name__ == "__main__":
    A = [2, 3, 4, 5, 7]
    sum = 0
    for a in A:
        sum += a
    print(sum)
