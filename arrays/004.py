"""Largest Sum Contiguous Subarray."""
"""
Write an efficient C program to find the sum of contiguous subarray within
a one-dimensional array of numbers which has the largest sum.

A = [-2, -3, 4, -1, -2, 1, 5, -3]
4 -1 -2 1 5 = 7 (Max)

Kadane's algorithm: https://www.youtube.com/watch?v=dVsspsEEHWE

"""

# Hard


def main(arr):
    """Largest sum contiguous subarray."""
    total_max = arr[0]  # Global maximum
    curr_max = arr[0]  # Maximum at each position

    for i in range(1, len(arr)):
        curr_max = max(arr[i], curr_max + arr[i])
        total_max = max(total_max, curr_max)
    print(total_max)
    return total_max


if __name__ == "__main__":
    A = [-2, -3, 4, -1, -2, 1, 5, -3]
    main(A)
