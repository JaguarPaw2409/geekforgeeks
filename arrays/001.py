"""Given an array A[] and a number x, check for pair in A[] with sum as x."""

"""Write a C program that, given an array A[] of n numbers and another number
x, determines whether or not there exist two elements in S whose sum is
exactly x."""

# Easy


def main(arr, sum_var):
    """Print summable numbers."""
    hash_var = set()
    for a in arr:
        diff = sum_var - a
        if diff in hash_var:
            print(a, diff)
        hash_var.add(a)


if __name__ == "__main__":
    A = [1, 4, 45, 6, 10, -8]
    n = 16
    main(A, n)
